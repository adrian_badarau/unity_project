﻿using UnityEngine;
using System.Collections;

public class DestroyByContact : MonoBehaviour {

	public GameObject explosion;
	public GameObject playerExplosion;
	private int scoreValue;
	private GameController gameController;

	void Start()
	{
		GameObject gameControllerObject = GameObject.FindWithTag ("GameController");
		this.scoreValue = 10;
		if (gameControllerObject != null) {
			gameController = gameControllerObject.GetComponent <GameController> ();
		} 
		if (gameController == null) {
			Debug.Log ("Can't find gameController");
		}
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.tag == "boundry") {
			return;
		}
		Destroy (other.gameObject);
		Instantiate (explosion, transform.position, transform.rotation);
		if (other.tag == "Player") {
			Instantiate (playerExplosion, other.transform.position, other.transform.rotation);
			gameController.GameOver();
		}
		Destroy (gameObject);
		gameController.AddScore (scoreValue);
	}
}
