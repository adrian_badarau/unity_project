﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameController : MonoBehaviour {

	public GameObject hazard;
	public Vector3 spawnValues;
	public int hazard_count;
	public float spawn_wait;
	public float start_wait;
	public float wave_wait;
	public Text scoreText;
	public Text restartText;
	public Text gameOverText;
	public int score;

	private bool gameOver;
	private bool restart;

	IEnumerator SpawnWaves()
	{
		yield return new WaitForSeconds (start_wait);
		while(!this.gameOver){
			for(int i=0; i< hazard_count; i++){
				Vector3 spawn_position = new Vector3 (Random.Range(-spawnValues.x, spawnValues.x), spawnValues.y, spawnValues.z);
				Quaternion spawn_rotation = Quaternion.identity;
				Instantiate (hazard, spawn_position, spawn_rotation);
				yield return new WaitForSeconds(spawn_wait);
			}
			yield return new WaitForSeconds(wave_wait);
			if (this.gameOver) {
				this.gameOverText.text = "";
				this.restartText.text = "Press 'R' for restart !";
				this.restart = true;
			}
		}
	}

	void Start()
	{
		this.score = 0;
		this.gameOver = false;
		this.restart = false;
		this.restartText.text = "";
		this.gameOverText.text = ""; 
		this.UpdateScore ();
		StartCoroutine( SpawnWaves ());
	}

	void Update()
	{
		if (this.restart) {
			if(Input.GetKeyDown( KeyCode.R)){
				Application.LoadLevel(Application.loadedLevel);
			}
		}
	}

	public void AddScore(int newValue)
	{
		this.score += newValue;
		this.UpdateScore ();  
	}

	void UpdateScore()
	{
		scoreText.text = "Score: " + score;
	}

	public void GameOver()
	{
		this.gameOverText.text = "You Died!";
		this.gameOver = true;
	}
}
