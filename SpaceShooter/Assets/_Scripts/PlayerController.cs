﻿using UnityEngine;
using System.Collections;

 [System.Serializable]
public class Boundary
{
	public float xMin,xMax,zMin,zMax;
}

public class PlayerController : MonoBehaviour {

	public float speed;
	public float tilt;
	public float fireRate = 0.5f;
	private float nextFire = 0.0f;
	private AudioSource audioSource;
	public Boundary boundary;
	public Transform shotSpawn;
	public GameObject shot;

	void Update()
	{
		if (Input.GetButton ("Fire1") && Time.time > nextFire) {
			nextFire = Time.time + fireRate;
			Instantiate (shot, shotSpawn.position, shotSpawn.rotation); 
			audioSource = GetComponent<AudioSource>();
			audioSource.Play();
		}

	}

	void FixedUpdate()
	{
		float moveHorizontal = Input.GetAxis ("Horizontal");
		float moveVertical = Input.GetAxis ("Vertical");

		Rigidbody rigidbody = GetComponent<Rigidbody>();
		rigidbody.velocity = new Vector3 (moveHorizontal, 0.0f, moveVertical)*speed;

		rigidbody.position = new Vector3
			(
				Mathf.Clamp(rigidbody.position.x, boundary.xMin, boundary.xMax),
				0.0f,
				Mathf.Clamp(rigidbody.position.z, boundary.zMin, boundary.zMax)
			);
		rigidbody.rotation = Quaternion.Euler (0.0f, 0.0f, rigidbody.velocity.x * -tilt);
	}

}
