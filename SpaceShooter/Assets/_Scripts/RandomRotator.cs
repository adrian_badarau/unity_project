﻿using UnityEngine;
using System.Collections;

public class RandomRotator : MonoBehaviour {

	public float tumble;

	void Start()
	{
		Rigidbody rigid_body = GetComponent<Rigidbody> ();
		rigid_body.angularVelocity = Random.insideUnitSphere * tumble;
 	}
}
