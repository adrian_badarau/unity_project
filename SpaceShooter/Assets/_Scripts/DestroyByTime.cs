﻿using UnityEngine;
using System.Collections;

public class DestroyByTime : MonoBehaviour {

	public float life_time;

	// Use this for initialization
	void Start () {
		Destroy (gameObject, life_time);
	}

}
